/* gui.cpp
 *
 * This is the source code for gui-only events such as initializing ncurses, 
 * refreshing windows, creating windows, displaying confirmation or error
 * messages, and formatting text.
 *
 * Code (c) Victor Dimitroff 2021
 * No part of this code may be copied or altered without explicit permission from the
 * copyright holder.
 */

#include "gui.hpp"

using std::string;
using std::vector;

WINDOW* acWindow;
WINDOW* mmWindow;
WINDOW* qdWindow;
WINDOW* qlWindow;
WINDOW* qtWindow;

const unsigned int CONFIRM = 10; //ENTER key code
int g_color = COLOR_WHITE; //default text color

/* Function    : get_color
 * Description : Returns the current color of the application text
 * Inputs      : void
 * Outputs     : color of application text as int
 */
int get_color() {
	return g_color;
}

/* Function    : set_color
 * Description : Sets the current color of the application text
 * Inputs      : nColor: new color as int
 * Outputs     : 0 if success
 */
int set_color(int nColor) {
	g_color = nColor;
	assume_default_colors(nColor, COLOR_BLACK);
	return 0;
}

/* Function    : refresh_all
 * Description : Refreshes all input windows
 * Inputs      : windows: windows to clear as Window* vector
 * Outputs     : 0 if success
 */
int refresh_all(vector<WINDOW*> windows) {
	for (std::size_t i = 0; i < windows.size(); i++) {
		touchwin(windows[i]);
		wrefresh(windows[i]);
	}
	return 0;
}

/* Function    : clear_all
 * Description : Clears and redraws a border around all input windows
 * Inputs      : windows: windows to clear as Window* vector
 * Outputs     : 0 if success
 */
int clear_all(vector<WINDOW*> windows) {
	for (std::size_t i = 0; i < windows.size(); i++) {
		wclear(windows[i]);
		box(windows[i], NULL, NULL);
	}
	return 0;
}

/* Function    : create_window
 * Description : Creates a new window, draws a border, and refreshes the window
 * Inputs      : height: new window height as int, width: new window width as int,
 *               starty: new window starting y position, startx: new window starting x position
 * Outputs     : pointer to new window
 */
WINDOW* create_window(int height, int width, int starty, int startx) {
	WINDOW* win = newwin(height, width, starty, startx);
	box(win, 0, 0);
	wrefresh(win);
	return win;
}

/* Function    : format_title_row
 * Description : Formats the titles for the quest list window
 * Inputs      : qid: quest id number as Id struct, title: quest title as string
 * Outputs     : formatted title as string
 */
string format_title_row(Id qid, string title) {
	string output = qid.type + std::to_string(qid.num) + ": " + title;
	return output.substr(0, qlWindow->_maxx - 4);
}

/* Function    : left_align
 * Description : Formats a string into a left aligned paragraph and prints it
 * Inputs      : window: window for text to be displayed as window pointer, str: input text as string
 * Outputs     : 0 if success
 */
int left_align(WINDOW* window, string str) {
	string line = "";
	string word = "";
	vector<string> lines = {};

	for (std::size_t i = 0; i < str.size(); i++) {
		if (str[i] == ' ') {
			if (line.size() + word.size() + 1 > (unsigned int) window->_maxx - 2) { //only a space will fit
				line += word;
				lines.push_back(line);
				line = "";
				word = "";
			}
			else if (i == str.size() - 1) { //end of input string
				line += word;
				lines.push_back(line);
			}
			else { //end of word
				word += ' ';
				line += word;
				word = "";
			}
		}
		else {
			if (line.size() + word.size() + 1 > (unsigned int) window->_maxx - 2) { //only a space will fit
				word += str[i];
				lines.push_back(line);
				line = "";
			}
			else if (i == str.size() - 1) { //end of input string
				word += str[i];
				line += word;
				lines.push_back(line);
			}
			else { //add char to word
				word += str[i];
			}
		}
	}
	
	for (std::size_t i = 0; i < lines.size(); i++) {
		mvwprintw(window, i + 1, 1, "%s", lines[i].c_str());
	}
	wrefresh(window);

	return 0;
}

/* Function    : left_align_str
 * Description : Formats a string into a left aligned paragraph
 * Inputs      : str: input text as string
 * Outputs     : lines to be printed as string vector
 */
vector<string> left_align_str(string str) {
	string line = "";
	string word = "";
	vector<string> lines = {};

	for (std::size_t i = 0; i < str.size(); i++) {
		if (str[i] == ' ') {
			if (line.size() + word.size() + 1 > (unsigned int) MAX_ALERT_WIDTH - 4) { //only a space will fit
				line += word;
				lines.push_back(line);
				line = "";
				word = "";
			}
			else if (i == str.size() - 1) { //end of input string
				line += word;
				lines.push_back(line);
			}
			else { //end of word
				word += ' ';
				line += word;
				word = "";
			}
		}
		else {
			if (line.size() + word.size() + 1 > (unsigned int) MAX_ALERT_WIDTH - 4) { //only a space will fit
				word += str[i];
				lines.push_back(line);
				line = "";
			}
			else if (i == str.size() - 1) { //end of input string
				word += str[i];
				line += word;
				lines.push_back(line);
			}
			else { //add char to word
				word += str[i];
			}
		}
	}

	for (std::size_t i = 0; i < lines.size(); i++) {
		while (lines[i].size() < (unsigned int) MAX_ALERT_WIDTH - 4) {
			lines[i] += " ";
		}
	}

	return lines;
}

/* Function    : left_align_objective
 * Description : Formats an objectived description into a left aligned paragraph
 * Inputs      : str: input text as string, len: available length to print as unsigned int
 *               loc: objective location as const string reference
 * Outputs     : lines to be printed as string vector
 */
vector<string> left_align_objective(string str, unsigned int len, const string &loc) {
	string word = "";
	vector<string> words = { "(" + loc + ") " };
	for (std::size_t i = 0; i < str.size(); i++) {
		if (str[i] == ' ') {
			word += str[i];
			words.push_back(word);
			word.clear();
		}
		else if (i == str.size() - 1) {
			word += str[i];
			words.push_back(word);
		}
		else {
			word += str[i];
		}
	}

	string line = "";
	vector<string> lines = {};
	for (std::size_t i = 0; i < words.size(); i++) {
		if (line.size() + 1 + words[i].size() > len) {
			lines.push_back(line);
			line.clear();
		}
		
		if (i == words.size() - 1) {
			line += words[i];
			lines.push_back(line);
		}
		else {
			line += words[i];
		}
	}

	return lines;
}

/* Function    : display_error
 * Description : Displays an error message to the user
 * Inputs      : message: error text as string
 * Outputs     : 0 if success
 */
int display_error(string message) {
	WINDOW* errorWindow;
	string output = message + " Press [ENTER] to continue.";
	vector<string> lines = { output };

	beep();
	if (output.size() < (unsigned int) MAX_ALERT_WIDTH - 4) {
		errorWindow = create_window(lines.size() + 2, output.size() + 4, 
			(stdscr->_maxy / 2) - (lines.size() / 2), (stdscr->_maxx / 2) - ((output.size() + 4) / 2));
	}
	else {
		lines = left_align_str(output);
		errorWindow = create_window(lines.size() + 2, MAX_ALERT_WIDTH, 
			(stdscr->_maxy / 2) - (lines.size() / 2), (stdscr->_maxx / 2) - (MAX_ALERT_WIDTH / 2));
	}

	wattron(errorWindow, COLOR_PAIR(ERROR));
	box(errorWindow, 0, 0);
	for (std::size_t i = 0; i < lines.size(); i++) {
		mvwprintw(errorWindow, i + 1, 1, " %s ", lines[i].c_str());
	}
	wattroff(errorWindow, COLOR_PAIR(ERROR));
	wrefresh(errorWindow);

	int inChar = wgetch(errorWindow);
	while (inChar != CONFIRM) {
		inChar = wgetch(errorWindow);
	}

	delwin(errorWindow);
	return 0;
}

/* Function    : display_confirmation
 * Description : Displays a confirmation message to the user
 * Inputs      : message: confirmation text as string
 * Outputs     : 0 if success
 */
int display_confirmation(string message) {
	WINDOW* confirmationWindow;
	string output = message + " Press [ENTER] to continue.";
	vector<string> lines = { output };

	if (output.size() < MAX_ALERT_WIDTH - 4) {
		confirmationWindow = create_window(lines.size() + 2, output.size() + 4, 
			(stdscr->_maxy / 2) - (lines.size() / 2), (stdscr->_maxx / 2) - ((output.size() + 4) / 2));
	}
	else {
		lines = left_align_str(output);
		confirmationWindow = create_window(lines.size() + 2, MAX_ALERT_WIDTH, 
			(stdscr->_maxy / 2) - (lines.size() / 2), (stdscr->_maxx / 2) - (MAX_ALERT_WIDTH / 2));
	}

	wattron(confirmationWindow, COLOR_PAIR(CONFIRMATION));
	box(confirmationWindow, 0, 0);
	for (std::size_t i = 0; i < lines.size(); i++) {
		mvwprintw(confirmationWindow, i + 1, 1, " %s ", lines[i].c_str());
	}
	wattroff(confirmationWindow, COLOR_PAIR(CONFIRMATION));
	wrefresh(confirmationWindow);

	int inChar = wgetch(confirmationWindow);
	while (inChar != CONFIRM) {
		inChar = wgetch(confirmationWindow);
	}

	delwin(confirmationWindow);
	return 0;
}

/* Function    : display_details
 * Description : Draws description and objectives for quest details window
 * Inputs      : quest: quest with details to be displayed as Quest
 * Outputs     : 0 if success
 */
int display_details(Quest &quest) {
	vector<Objective> objectives = quest.get_obj();
	
	werase(qtWindow);
	box(qtWindow, 0, 0);
	mvwaddstr(qtWindow, 1, (qtWindow->_maxx / 2) - (quest.get_title().length() / 2), quest.get_title().c_str());
	wrefresh(qtWindow);

	werase(qdWindow);
	box(qdWindow, 0, 0);
	left_align(qdWindow, quest.get_desc());
	wmove(qdWindow, getcury(qdWindow) + 1, 2);

	for (std::size_t i = 0; i < objectives.size(); i++) {
		if (objectives[i].get_completed() == false) {
			mvwprintw(qdWindow, getcury(qdWindow) + 1, 2, "[ ]");
		}
		else if (objectives[i].get_completed() == true) {
			mvwprintw(qdWindow, getcury(qdWindow) + 1, 2, "[x]");
		}

		mvwprintw(qdWindow, getcury(qdWindow), 6, "%c%i: ", objectives[i].get_oid().type, objectives[i].get_oid().num);
		
		unsigned int starty = getcury(qdWindow);
		unsigned int startx = getcurx(qdWindow);
		vector<string> lines = left_align_objective(objectives[i].get_desc(), qdWindow->_maxx - startx - 2, objectives[i].get_loc());
		for (std::size_t j = 0; j < lines.size(); j++) {
			mvwprintw(qdWindow, starty + j, startx, "%s", lines[j].c_str());
		}
	}

	wrefresh(qdWindow);
	return 0;
}

/* Function    : display_details
 * Description : Draws description and objectives for objective selection mode
 * Inputs      : quest: quest with details to be displayed as Quest, oSelection: current objective selection as int
 * Outputs     : 0 if success
 */
int display_details(Quest &quest, int oSelection) {
	vector<Objective> objectives = quest.get_obj();
	
	werase(qdWindow);
	box(qdWindow, 0, 0);
	left_align(qdWindow, quest.get_desc());
	wmove(qdWindow, getcury(qdWindow) + 1, 2);

	for (std::size_t i = 0; i < objectives.size(); i++) {
		if (i == oSelection) { //selected objective
			wattron(qdWindow, A_STANDOUT);
			if (objectives[i].get_completed() == false) {
				mvwprintw(qdWindow, getcury(qdWindow) + 1, 2, "[ ]");
			}
			else if (objectives[i].get_completed() == true) {
				mvwprintw(qdWindow, getcury(qdWindow) + 1, 2, "[x]");
			}
			wattroff(qdWindow, A_STANDOUT);
		}
		else {
			if (objectives[i].get_completed() == false) {
				mvwprintw(qdWindow, getcury(qdWindow) + 1, 2, "[ ]");
			}
			else if (objectives[i].get_completed() == true) {
				mvwprintw(qdWindow, getcury(qdWindow) + 1, 2, "[x]");
			}
		}
		mvwprintw(qdWindow, getcury(qdWindow), 6, "%c%i: ", objectives[i].get_oid().type, objectives[i].get_oid().num);

		unsigned int starty = getcury(qdWindow);
		unsigned int startx = getcurx(qdWindow);
		vector<string> lines = left_align_objective(objectives[i].get_desc(), qdWindow->_maxx - startx - 2, objectives[i].get_loc());
		for (std::size_t j = 0; j < lines.size(); j++) {
			mvwprintw(qdWindow, starty + j, startx, "%s", lines[j].c_str());
		}
	}

	if (quest.get_obj().size() == 0) { //formatted for quests with no objectives
		mvwhline(qdWindow, getcury(qdWindow) + 1, (qdWindow->_maxx / 2) - 9, ACS_HLINE, 18);
		mvwprintw(qdWindow, getcury(qdWindow) + 1, (qdWindow->_maxx / 2) - 8, "[C]OMPLETE QUEST");
		mvwhline(qdWindow, getcury(qdWindow) + 1, (qdWindow->_maxx / 2) - 9, ACS_HLINE, 18);
	}
	else { //formatted for quests with objectives
		mvwhline(qdWindow, getcury(qdWindow) + 2, (qdWindow->_maxx / 2) - 9, ACS_HLINE, 18);
		mvwprintw(qdWindow, getcury(qdWindow) + 1, (qdWindow->_maxx / 2) - 8, "[C]OMPLETE QUEST");
		mvwhline(qdWindow, getcury(qdWindow) + 1, (qdWindow->_maxx / 2) - 9, ACS_HLINE, 18);
	}

	wrefresh(qdWindow);
	return 0;
}

/* Function    : init_curses
 * Description : Initializes the curses display
 * Inputs      : void
 * Outputs     : 0 if success
 */
int init_curses() {
	initscr();
	noecho();
	keypad(stdscr, TRUE);
	curs_set(false);
	resize_term(41, 126);
	
	start_color();
	assume_default_colors(COLOR_WHITE, COLOR_BLACK);
	init_pair(ERROR, COLOR_YELLOW, COLOR_RED);
	init_pair(CONFIRMATION, COLOR_WHITE, COLOR_GREEN);

	acWindow = create_window(3, 34, 0, 0); //active-completed window
	mvwaddstr(acWindow, 1, 5, "[A]CTIVE");
	mvwaddstr(acWindow, 1, (34 / 2), "|");
	mvwaddstr(acWindow, 1, (34 / 2) + 3, "[C]OMPLETED");
	wrefresh(acWindow);

	mmWindow = create_window(3, 126, 38, 0); //main menu window
	mvwaddstr(mmWindow, 1, 1 + 7, "Add a [N]ew Quest");
	mvwaddstr(mmWindow, 1, 8 + 18 + 7, "[S]ave Quests to File");
	mvwaddstr(mmWindow, 1, 8 + 25 + 22 + 7, "[L]oad Quests from File");
	mvwaddstr(mmWindow, 1, 8 + 25 + 29 + 24 + 7, "[O]ptions");
	mvwaddstr(mmWindow, 1, 8 + 25 + 29 + 31 + 10 + 7, "E[x]it!");
	wrefresh(mmWindow);
	
	qdWindow = create_window(35, 92, 3, 34); //quest details window
	wrefresh(qdWindow);

	qlWindow = create_window(35, 34, 3, 0); //quest list window
	wrefresh(qlWindow);

	qtWindow = create_window(3, 92, 0, 34); //quest title window
	wrefresh(qtWindow);

	return 0;
}