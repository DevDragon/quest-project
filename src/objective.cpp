/* objective.cpp
 *
 * This is the source code file for the Objective class, detailing the implementations
 * for the contructors, getters, setters, and operator overloads.
 *
 * Code (c) Victor Dimitroff 2021
 * No part of this code may be copied or altered without explicit permission from the
 * copyright holder.
 */

#include <iostream>
#include "objective.hpp"

using std::string;

/* Function    : Objective
 * Description : Default constructor
 * Inputs      : void
 * Outputs     : void
 */
Objective::Objective() {
	oid = { 'o', 0 };
	completed = false;
	loc = "";
	desc = "";
}

/* Function    : Objective
 * Description : Parameterized constructor
 * Inputs      : noid: new objective id as Id struct, ncompleted: new completed flag as boolean,
 *               nloc: new objective location as string, ndesc: new objective description as string
 * Outputs     : void
 */
Objective::Objective(Id noid, bool ncompleted, string nloc, string ndesc) { //constructor definition
	oid = noid;
	completed = ncompleted;
	loc = nloc;
	desc = ndesc;
}

/* Function    : get_oid
 * Description : Objective id getter
 * Inputs      : void
 * Outputs     : objective id as Id struct
 */
Id Objective::get_oid(void) {
	return oid;
}

/* Function    : get_completed
 * Description : Objective completed getter
 * Inputs      : void
 * Outputs     : completed flag as boolean
 */
bool Objective::get_completed(void) {
	return completed;
}

/* Function    : get_loc
 * Description : Objective location getter
 * Inputs      : void
 * Outputs     : location as string
 */
string Objective::get_loc(void) {
	return loc;
}

/* Function    : get_desc
 * Description : Objective description getter
 * Inputs      : void
 * Outputs     : description as string
 */
string Objective::get_desc(void) {
	return desc;
}

/* Function    : set_oid
 * Description : Objective id setter
 * Inputs      : newOid: new objective id as Id struct
 * Outputs     : void
 */
void Objective::set_oid(Id newOid) {
	oid = newOid;
}

/* Function    : set_completed
 * Description : Objective completed setter
 * Inputs      : newCompleted: new objective completed flag as a boolean
 * Outputs     : void
 */
void Objective::set_completed(bool newCompleted) {
	completed = newCompleted;
}

/* Function    : set_loc
 * Description : Objective location setter
 * Inputs      : newLoc: new objective location as string
 * Outputs     : void
 */
void Objective::set_loc(string newLoc) {
	loc = newLoc;
}

/* Function    : set_desc
 * Description : Objective description setter
 * Inputs      : newDesc: new objective description as string
 * Outputs     : void
 */
void Objective::set_desc(string newDesc) {
	desc = newDesc;
}

/* Function    : operator<<
 * Description : Objective << operator overlaod
 * Inputs      : out: stream object to output as reference,
 *               obj: objective input as const Objective reference
 * Outputs     : output stream as reference
 */
std::ostream &operator<<(std::ostream &out, const Objective &obj) {
	out << "[" << "\n";
	out << obj.oid.type << "\n";
	out << std::to_string(obj.oid.num) << "\n";
	out << obj.completed << "\n";
	out << obj.loc << "\n";
	out << obj.desc << "\n";
	out << "]" << "\n";

	return out;
}

/* Function    : operator>>
 * Description : Objective >> operator overload
 * Inputs      : in: stream object to input as reference,
 *               obj: objective input as Objective reference 
 * Outputs     : input stream as reference
 */
std::istream &operator>>(std::istream &in, Objective &obj) {
	in >> obj.oid.type;
	in >> obj.oid.num;
	in >> obj.completed;
	in.ignore(1);
	getline(in, obj.loc);
	getline(in, obj.desc);

	return in;
}