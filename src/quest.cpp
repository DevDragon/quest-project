/* quest.cpp
 *
 * This is the source code file for the Quest class, detailing the implementations
 * for the contructors, getters, setters, and operator overloads.
 *
 * Code (c) Victor Dimitroff 2021
 * No part of this code may be copied or altered without explicit permission from the
 * copyright holder.
 */

#include <iostream>
#include "quest.hpp"

using std::string;
using std::vector;

/* Function    : Quest
 * Description : Default constructor
 * Inputs      : void
 * Outputs     : void
 */
Quest::Quest(void) {
	qid = { 'q', UINT_MAX };
	completed = false;
	title = "";
	desc = "";
	objectives = { };
}

/* Function    : Quest
 * Description : Parameterized constructor
 * Inputs      : nqid: new quest id as Id struct, ncompleted: new quest completed flag as boolean,
 *               ntitle: new quest title as string, ndesc: new quest description as string,
 *               nobjectives: new quest objectives as Objectives vector
 * Outputs     : void
 */
Quest::Quest(Id nqid, bool ncompleted, string ntitle, string ndesc, vector<Objective> nobjectives) {
	qid = nqid;
	completed = ncompleted;
	title = ntitle;
	desc = ndesc;
	objectives = nobjectives;
}

/* Function    : get_qid
 * Description : Quest id getter
 * Inputs      : void
 * Outputs     : quest id as Id struct
 */
Id Quest::get_qid(void) {
	return qid;
}

/* Function    : get_completed
 * Description : Quest completed flag getter
 * Inputs      : void
 * Outputs     : quest completed flag as boolean
 */
bool Quest::get_completed(void) {
	return completed;
}

/* Function    : get_title
 * Description : Quest title getter
 * Inputs      : void
 * Outputs     : quest title as string
 */
string Quest::get_title(void) {
	return title;
}

/* Function    : get_desc
 * Description : Quest description getter
 * Inputs      : void
 * Outputs     : quest description as string
 */
string Quest::get_desc(void) {
	return desc;
}

/* Function    : get_obj
 * Description : Quest objectives getter
 * Inputs      : void
 * Outputs     : quest objectives as Objectives vector
 */
vector<Objective> Quest::get_obj(void) {
	return objectives;
}

/* Function    : set_qid
 * Description : Quest id setter
 * Inputs      : newQid: new quest id as Id struct
 * Outputs     : void
 */
void Quest::set_qid(Id newQid) {
	qid = newQid;
}

/* Function    : set_completed
 * Description : Quest completed flag setter
 * Inputs      : newCompleted: new quest completed flag as boolean
 * Outputs     : void
 */
void Quest::set_completed(bool newCompleted) {
	completed = newCompleted;
}

/* Function    : set_title
 * Description : Quest title setter
 * Inputs      : newTitle: new quest title as string
 * Outputs     : void
 */
void Quest::set_title(string newTitle) {
	title = newTitle;
}

/* Function    : set_desc
 * Description : Quest description setter
 * Inputs      : newDesc: new quest description as string
 * Outputs     : void
 */
void Quest::set_desc(string newDesc) {
	desc = newDesc;
}

/* Function    : set_obj
 * Description : Quest objectives setter
 * Inputs      : newObjectives: new quest objectives as Objective vector
 * Outputs     : void
 */
void Quest::set_obj(const vector<Objective> &newObjectives) {
	objectives = newObjectives;
}

/* Function    : operator<<
 * Description : Quest << operator overload
 * Inputs      : out: output stream as reference,
 *               obj: quest object input as reference
 * Outputs     : output stream reference
 */
std::ostream &operator<<(std::ostream &out, const Quest &obj) {
	out << "{" << "\n";
	out << obj.qid.type << "\n";
	out << std::to_string(obj.qid.num) << "\n";
	out << obj.completed << "\n";
	out << obj.title << "\n";
	out << obj.desc << "\n";

	for (std::size_t i = 0; i < obj.objectives.size(); i++) {
		out << obj.objectives[i];
	}

	out << "}" << "\n";

	return out;
}

/* Function    : operator>>
 * Description : Quest >> operator overload
 * Inputs      : in: input stream as reference,
 *               obj: quest object input as reference
 * Outputs     : input stream reference
 */
std::istream &operator>>(std::istream &in, Quest &obj) {
	in >> obj.qid.type;
	in >> obj.qid.num;
	in >> obj.completed;
	in.ignore(1);
	getline(in, obj.title);
	getline(in, obj.desc);

	Objective newObjective;
	string line = "";
	while (getline(in, line)) {
		if (line == "[") {
			in >> newObjective;
			obj.objectives.push_back(newObjective);
		}
		else if (line == "}") {
			break;
		}
	}

	return in;
}