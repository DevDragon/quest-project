/* main.cpp
 *
 * This is the main source code for the program. The file includes implementations
 * for helper functions like is_allowed_char and check_unique_qid as well as 
 * implementations for all primary program functionality.
 *
 * Code (c) Victor Dimitroff 2021
 * No part of this code may be copied or altered without explicit permission from the
 * copyright holder.
 */

#include <csignal>
#include <iostream>
#include <fstream>
#include <filesystem>
#include "gui.hpp"
#include "quest_project.hpp"

using std::string;
using std::vector;

int g_start = 0; //starting quest to print in quest list

/* Function    : is_allowed_char
 * Description : Checks if character is allowed to be entered for title or description
 * Inputs      : inChar: character to be checked as int
 * Outputs     : true if success, false if failure
 */
bool is_allowed_char(int inChar) {
	if (isalnum(inChar)) {
		return true;
	}
	else if (isspace(inChar)) {
		return true;
	}
	else {
		switch (inChar) {
		case '.':
			return true;
		case '?':
			return true;
		case '!':
			return true;
		case '\'':
			return true;
		case '"':
			return true;
		case '#':
			return true;
		case '&':
			return true;
		case ',':
			return true;
		case ':':
			return true;
		case '-':
			return true;
		case '/':
			return true;
		default:
			return false;
		}
	}
	return false;
}

/* Function    : check_unique_qid
 * Description : Checks if entered qid is already used
 * Inputs      : quests: quests as Quest vector, number: number to check as int
 * Outputs     : true if success, false if failure
 */
bool check_unique_qid(vector<Quest>* quests, int number) {
	for (vector<Quest>::iterator i = quests->begin(); i != quests->end(); ++i) {
		if (i->get_qid().num == number) {
			return false;
		}
	}
	return true;
}

/* Function    : view_completed_quests
 * Description : Updates the quest list to display completed quests
 * Inputs      : quests: quests as Quest vector reference, 
                 completedQuests: completed quest indices as int vector,
				 qSelection: current quest selection as int
 * Outputs     : 0 if success, -1 if failure or no completed quests
 */
int view_completed_quests(vector<Quest> &quests, vector<int> completedQuests, int qSelection) {
	if (qSelection - g_start == MAX_LIST_SIZE) {
		g_start++;
	}
	else if (qSelection == g_start - 1) {
		g_start--;
	}
	
	mvwaddstr(acWindow, 1, 5, "[A]CTIVE");

	wattron(acWindow, A_STANDOUT);
	mvwaddstr(acWindow, 1, (34 / 2) + 3, "[C]OMPLETED");
	wattroff(acWindow, A_STANDOUT);
	wrefresh(acWindow);

	werase(qlWindow);
	box(qlWindow, NULL, NULL);

	if (completedQuests.size() == 0) { //no completed quests
		mvwprintw(qlWindow, 1, 1, "+------------------------------+");
		mvwprintw(qlWindow, 2, 1, "| You have no completed quests.|");
		mvwprintw(qlWindow, 3, 1, "| Please select another option |");
		mvwprintw(qlWindow, 4, 1, "| from the Main Menu.          |");
		mvwprintw(qlWindow, 5, 1, "+------------------------------+");

		wrefresh(qlWindow);

		return -1;
	}

	for (std::size_t i = 0; i < MAX_LIST_SIZE && i < completedQuests.size(); i++) {
		Id qid = quests[completedQuests[g_start + i]].get_qid();
		string title = quests[completedQuests[g_start + i]].get_title();

		if (g_start + i == qSelection) {
			wattron(qlWindow, A_STANDOUT);
			mvwaddstr(qlWindow, (i * 2 + 1), 2, format_title_row(qid, title).c_str());
			wattroff(qlWindow, A_STANDOUT);
		}
		else {
			mvwaddstr(qlWindow, (i * 2 + 1), 2, format_title_row(qid, title).c_str());
		}
		
		if (i >= MAX_LIST_SIZE - 1) {
			wrefresh(qlWindow);
			break;
		}
		else {
			mvwaddstr(qlWindow, (i * 2 + 2), 1, "+------------------------------+");
		}

		wrefresh(qlWindow);
	}

	return 0;
}

/* Function    : view_active_quests
 * Description : Updates the quest list to display active quests
 * Inputs      : quests: quests as Quest vector reference,
				 activeQuests: active quest indices as int vector,
				 qSelection: current quest selection as int
 * Outputs     : 0 if success, -1 if failure or no active quests
 */
int view_active_quests(vector<Quest> &quests, vector<int> activeQuests, int qSelection) {
	if (qSelection - g_start == MAX_LIST_SIZE) {
		g_start++;
	}
	else if (qSelection == g_start - 1) {
		g_start--;
	}
	
	wattron(acWindow, A_STANDOUT);
	mvwaddstr(acWindow, 1, 5, "[A]CTIVE");
	wattroff(acWindow, A_STANDOUT);

	mvwaddstr(acWindow, 1, (34 / 2) + 3, "[C]OMPLETED");
	wrefresh(acWindow);

	wclear(qlWindow);
	box(qlWindow, NULL, NULL);

	if (activeQuests.size() == 0) { //no active quests
		mvwprintw(qlWindow, 1, 1, "+------------------------------+");
		mvwprintw(qlWindow, 2, 1, "| You have no active quests.   |");
		mvwprintw(qlWindow, 3, 1, "| Please select another option |");
		mvwprintw(qlWindow, 4, 1, "| from the Main Menu.          |");
		mvwprintw(qlWindow, 5, 1, "+------------------------------+");
		
		wrefresh(qlWindow);
		
		return -1;
	}
	
	for (std::size_t i = 0; i < MAX_LIST_SIZE && i < activeQuests.size(); i++) {
		Id qid = quests[activeQuests[g_start + i]].get_qid();
		string title = quests[activeQuests[g_start + i]].get_title();

		if (g_start + i == qSelection) {
			wattron(qlWindow, A_STANDOUT);
			mvwaddstr(qlWindow, (i * 2 + 1), 2, format_title_row(qid, title).c_str());
			wattroff(qlWindow, A_STANDOUT);
		}
		else {
			mvwaddstr(qlWindow, (i * 2 + 1), 2, format_title_row(qid, title).c_str());
		}
		
		if (i >= MAX_LIST_SIZE - 1) {
			wrefresh(qlWindow);
			break;
		}
		else {
			mvwaddstr(qlWindow, (i * 2 + 2), 1, "+------------------------------+");
		}
		
		wrefresh(qlWindow);
	}

	return 0;
}

/* Function    : get_quests_status
 * Description : Updates the activeQuests and completedQuests vectors
 * Inputs      : quests: quests as Quest vector reference,
				 activeQuests: active quest indices as int vector pointer,
				 completedQuests: completed quest indices as int vector pointer
 * Outputs     : 0 if success
 */
int get_quests_status(vector<Quest> &quests, vector<int> *activeQuests, vector<int> *completedQuests) {
	// Clear lists of active and completed indices and refresh
	activeQuests->clear();
	completedQuests->clear();
	for (std::size_t i = 0; i < quests.size(); i++) {
		if (quests[i].get_completed() == false) {
			activeQuests->push_back(i);
		}
		else if (quests[i].get_completed() == true) {
			completedQuests->push_back(i);
		}
	}
	return 0;
}

/* Function    : create_objective
 * Description : Creates a new objective
 * Inputs      : objectives: objectives as Objective vector pointer,
 *               nqWindow: new quest window as WINDOW pointer
 * Outputs     : 0 if success, -1 if failure or cancelled
 */
int create_objective(vector<Objective>* objectives, WINDOW* nqWindow) {
	Objective newObjective;
	int omSelection = 0;
	int dSelection = 1;
	int inChar = 0;
	string newLoc = "";
	string newDesc = "";
	string numStr = "";

	WINDOW* noWindow = create_window(17, 59, 12, 33);
	mvwaddstr(noWindow, 1, 19, "CREATE A NEW OBJECTIVE");
	mvwhline(noWindow, 2, 1, ACS_HLINE, 57);

	mvwprintw(noWindow, 4, 2, "Objective ID: o%i", objectives->size());
	wattron(noWindow, A_STANDOUT);
	mvwaddstr(noWindow, 7, 2, "Objective Location:");
	wattroff(noWindow, A_STANDOUT);
	mvwaddstr(noWindow, 11, 2, "Objective Description:");

	vector<string> wContent = { "Objective Id:", "Objective Location:", "Objective Description:" };

	mvwhline(noWindow, 14, 1, ACS_HLINE, 57);
	mvwaddstr(noWindow, 15, 2, "[C]ancel");
	mvwaddstr(noWindow, 15, 51, "[D]one");
	wrefresh(noWindow);

	WINDOW* locEntry = create_window(3, 35, 18, 55);
	wrefresh(locEntry);

	WINDOW* descEntry = create_window(5, 32, 21, 58);
	wrefresh(descEntry);

	while (true) {
		omSelection = getch();

		if (is_termresized()) {
			resize_term(41, 126);
			curs_set(false);
			refresh_all({ qtWindow, qdWindow, qlWindow, mmWindow, acWindow, noWindow, locEntry, descEntry });
		}

		switch (omSelection) {
		case CANCEL:
			return -1;
		case DONE:
			if (newObjective.get_oid().num >= 0 && newObjective.get_loc() != "" && newObjective.get_desc() != "") {
				newObjective.set_oid({ 'o', (unsigned int) objectives->size() });
				objectives->push_back(newObjective);
				return 0;
			}
			else {
				display_error("There was a problem creating an objective! Please make sure that all entries are populated and valid.");
				refresh_all({ qlWindow, qdWindow, nqWindow, noWindow, locEntry, descEntry });
			}
			break;
		case CONFIRM: //enter key is pressed
			curs_set(true);
			if (dSelection == TITLELOC) { //location entry
				werase(locEntry);
				wattron(locEntry, A_STANDOUT);
				box(locEntry, NULL, NULL);
				wattroff(locEntry, A_STANDOUT);
				wrefresh(locEntry);
				wmove(locEntry, 1, 1);

				newLoc = "";
				inChar = wgetch(locEntry);
				while (inChar != CONFIRM) {
					if (is_allowed_char(inChar)) {
						waddch(locEntry, inChar);
						newLoc += inChar;
						wrefresh(locEntry);
					}
					else if (inChar == '\b' && newLoc.size() > 0) {
						mvwaddch(locEntry, getcury(locEntry), getcurx(locEntry) - 1, ' ');
						wmove(locEntry, getcury(locEntry), getcurx(locEntry) - 1);
						newLoc.pop_back();
						wrefresh(locEntry);
					}
					inChar = wgetch(locEntry);
				}

				newObjective.set_loc(newLoc);

				werase(locEntry);
				box(locEntry, NULL, NULL);
				mvwprintw(locEntry, 1, 1, "%s", newObjective.get_loc().c_str());
				wrefresh(locEntry);
			}
			else if (dSelection == DESC) { //description entry
				werase(descEntry);
				wattron(descEntry, A_STANDOUT);
				box(descEntry, NULL, NULL);
				wattroff(descEntry, A_STANDOUT);
				wrefresh(descEntry);
				wmove(descEntry, 1, 1);

				newDesc = "";
				inChar = wgetch(descEntry);
				while (inChar != CONFIRM) {
					if (getcurx(descEntry) == descEntry->_maxx - 2 && getcury(descEntry) < descEntry->_maxy - 1) {
						wmove(descEntry, getcury(descEntry) + 1, 1);
					}

					if (is_allowed_char(inChar) && getcury(descEntry) < descEntry->_maxy - 1) {
						waddch(descEntry, inChar);
						newDesc += inChar;
						wrefresh(descEntry);
					}
					else if (inChar == '\b' && newDesc.size() > 0) {
						if (getcurx(descEntry) == 1 && getcury(descEntry) > 1) {
							wmove(descEntry, getcury(descEntry) - 1, descEntry->_maxx - 2);
						}
						else if (getcurx(descEntry) == 1 && getcury(descEntry) == 1) {
							break;
						}

						mvwaddch(descEntry, getcury(descEntry), getcurx(descEntry) - 1, ' ');
						wmove(descEntry, getcury(descEntry), getcurx(descEntry) - 1);
						newDesc.pop_back();
						wrefresh(descEntry);
					}
					inChar = wgetch(descEntry);
				}

				newObjective.set_desc(newDesc);

				werase(descEntry);
				box(descEntry, NULL, NULL);
				left_align(descEntry, newObjective.get_desc());
				wrefresh(descEntry);
			}
			curs_set(false);
			break;
		case KEY_DOWN:
			if ((unsigned int) dSelection + 1 < wContent.size()) {
				if (dSelection == DESC) {
					mvwaddstr(noWindow, (dSelection * 3) + 5, 2, wContent[dSelection].c_str());
				}
				else {
					mvwaddstr(noWindow, (dSelection * 3) + 4, 2, wContent[dSelection].c_str());
				}

				dSelection++;

				wattron(noWindow, A_STANDOUT);
				if (dSelection == DESC) {
					mvwaddstr(noWindow, (dSelection * 3) + 5, 2, wContent[dSelection].c_str());
				}
				else {
					mvwaddstr(noWindow, (dSelection * 3) + 4, 2, wContent[dSelection].c_str());
				}
				wattroff(noWindow, A_STANDOUT);

				wrefresh(noWindow);
			}
			break;
		case KEY_UP:
			if (dSelection - 1 >= 1) {
				if (dSelection == DESC) {
					mvwaddstr(noWindow, (dSelection * 3) + 5, 2, wContent[dSelection].c_str());
				}
				else {
					mvwaddstr(noWindow, (dSelection * 3) + 4, 2, wContent[dSelection].c_str());
				}

				dSelection--;

				wattron(noWindow, A_STANDOUT);
				if (dSelection == DESC) {
					mvwaddstr(noWindow, (dSelection * 3) + 5, 2, wContent[dSelection].c_str());
				}
				else {
					mvwaddstr(noWindow, (dSelection * 3) + 4, 2, wContent[dSelection].c_str());
				}
				wattroff(noWindow, A_STANDOUT);

				wrefresh(noWindow);
			}
			break;
		default:
			break;
		}
	}
	return -1;
}

/* Function    : create_quest
 * Description : Creates a new quest
 * Inputs      : quests: quests as Quest vector pointer
 * Outputs     : 0 if success, -1 if failure or cancelled
 */
int create_quest(vector<Quest> *quests) {
	Quest newQuest;
	int nmSelection = 0;
	int dSelection = 0;
	int inChar = 0;
	string newTitle = "";
	string newDesc = "";
	string numStr = "";
	vector<Objective> newObjectives = {};

	WINDOW* nqWindow = create_window(21, 63, 10, 31);
	mvwaddstr(nqWindow, 1, 23, "CREATE A NEW QUEST");
	mvwhline(nqWindow, 2, 1, ACS_HLINE, 61);

	wattron(nqWindow, A_STANDOUT);
	mvwaddstr(nqWindow, 4, 2, "Quest ID:");
	wattroff(nqWindow, A_STANDOUT);
	mvwaddstr(nqWindow, 7, 2, "Quest Title:");
	mvwaddstr(nqWindow, 12, 2, "Quest Description:");
	
	mvwaddstr(nqWindow, 16, 2, "Objectives:");
	mvwaddstr(nqWindow, 16, 14, std::to_string(newQuest.get_obj().size()).c_str());
	
	vector<string> wContent = { "Quest ID:", "Quest Title:", "Quest Description:" };

	mvwhline(nqWindow, 18, 1, ACS_HLINE, 61);
	mvwaddstr(nqWindow, 19, 2, "[C]ancel");
	mvwaddstr(nqWindow, 19, 25, "[N]ew Objective");
	mvwaddstr(nqWindow, 19, 55, "[D]one");
	wrefresh(nqWindow);

	WINDOW* qidEntry = create_window(3, 50, 13, 43);
	mvwaddstr(qidEntry, 1, 1, "q");
	wrefresh(qidEntry);

	WINDOW* titleEntry = create_window(3, 47, 16, 46);
	wrefresh(titleEntry);

	WINDOW* descEntry = create_window(7, 41, 19, 52);
	wrefresh(descEntry);
	
	while (true) {
		int nmSelection = getch();

		if (is_termresized()) {
			resize_term(41, 126);
			curs_set(false);
			refresh_all({ qtWindow, qdWindow, qlWindow, mmWindow, acWindow, nqWindow, qidEntry, titleEntry, descEntry });
		}

		switch (nmSelection) {
			case CANCEL:
				return -1;
			case NEWOB:
				if (create_objective(&newObjectives, nqWindow) == 1) {
					display_confirmation("Objective added!");
				}
				newQuest.set_obj(newObjectives);
				mvwaddstr(nqWindow, 16, 14, std::to_string(newQuest.get_obj().size()).c_str());
				refresh_all({ nqWindow, qidEntry, titleEntry, descEntry });
				break;
			case DONE:
				if (newQuest.get_qid().num == UINT_MAX) //qid wasn't changed
				{
					//TODO: get lowest available quest id
					vector<unsigned int> takenNums;
					for (vector<Quest>::iterator i = quests->begin(); i != quests->end(); i++)
					{
						takenNums.push_back(i->get_qid().num);
					}

					unsigned int newQid = 0;
					while (newQuest.get_qid().num == UINT_MAX)
					{
						if (find(takenNums.begin(), takenNums.end(), newQid) != takenNums.end())
						{
							newQid++;
						}
						else
						{
							newQuest.set_qid({ 'q', newQid });
						}
					}
				}

				if (newQuest.get_qid().num != UINT_MAX && newQuest.get_title() != "" && newQuest.get_desc() != "") {
					quests->push_back(newQuest);
					return 0;
				}
				else {
					display_error("There was a problem creating a quest! Please make sure that all entries are populated and valid.");
					refresh_all({ qlWindow, qdWindow, nqWindow, qidEntry, titleEntry, descEntry });
					break;
				}
				break;
			case CONFIRM: //enter key is pressed
				curs_set(true);
				if (dSelection == ID) { //qid entry
					werase(qidEntry);
					wattron(qidEntry, A_STANDOUT);
					box(qidEntry, NULL, NULL);
					wattroff(qidEntry, A_STANDOUT);
					mvwaddstr(qidEntry, 1, 1, "q");
					wrefresh(qidEntry);
					wmove(qidEntry, 1, 2);

					numStr = "";
					inChar = wgetch(qidEntry);
					while (inChar != CONFIRM) {
						if (isdigit(inChar)) {
							waddch(qidEntry, inChar);
							numStr += (char)inChar;
							wrefresh(qidEntry);
						}
						else if (inChar == '\b' && numStr.size() > 0) {
							mvwaddch(qidEntry, getcury(qidEntry), getcurx(qidEntry) - 1, ' ');
							wmove(qidEntry, getcury(qidEntry), getcurx(qidEntry) - 1);
							numStr.pop_back();
							wrefresh(qidEntry);
						}
						inChar = wgetch(qidEntry);
					}

					if (numStr == "") { //field is empty
						werase(qidEntry);
						box(qidEntry, NULL, NULL);
						mvwprintw(qidEntry, 1, 1, "%c", newQuest.get_qid().type);
						wrefresh(qidEntry);
						curs_set(false);
						break;
					}

					if (check_unique_qid(quests, stoi(numStr)) == false) { //validate that qid is unique
						curs_set(false);
						
						display_error("Quest ID must be unique.");
						refresh_all({ nqWindow, qidEntry, titleEntry, descEntry });
						
						werase(qidEntry);
						box(qidEntry, NULL, NULL);
						mvwprintw(qidEntry, 1, 1, "%c", newQuest.get_qid().type);
						wrefresh(qidEntry);
						break;
					}
					
					newQuest.set_qid({ 'q', (unsigned int) stoi(numStr) });

					werase(qidEntry);
					box(qidEntry, NULL, NULL);
					mvwprintw(qidEntry, 1, 1, "%c%i", newQuest.get_qid().type, newQuest.get_qid().num);
					wrefresh(qidEntry);
				}
				else if (dSelection == TITLELOC) { //title entry
					werase(titleEntry);
					wattron(titleEntry, A_STANDOUT);
					box(titleEntry, NULL, NULL);
					wattroff(titleEntry, A_STANDOUT);
					wrefresh(titleEntry);
					wmove(titleEntry, 1, 1);

					newTitle = "";
					inChar = wgetch(titleEntry);
					while (inChar != CONFIRM) {
						if (is_allowed_char(inChar)) {
							waddch(titleEntry, inChar);
							newTitle += inChar;
							wrefresh(titleEntry);
						}
						else if (inChar == '\b' && newTitle.size() > 0) {
							mvwaddch(titleEntry, getcury(titleEntry), getcurx(titleEntry) - 1, ' ');
							wmove(titleEntry, getcury(titleEntry), getcurx(titleEntry) - 1);
							newTitle.pop_back();
							wrefresh(titleEntry);
						}
						inChar = wgetch(titleEntry);
					}

					newQuest.set_title(newTitle);

					werase(titleEntry);
					box(titleEntry, NULL, NULL);
					mvwprintw(titleEntry, 1, 1, "%s", newQuest.get_title().c_str());
					wrefresh(titleEntry);
				}
				else if (dSelection == DESC) { //description entry
					werase(descEntry);
					wattron(descEntry, A_STANDOUT);
					box(descEntry, NULL, NULL);
					wattroff(descEntry, A_STANDOUT);
					wrefresh(descEntry);
					wmove(descEntry, 1, 1);

					newDesc = "";
					inChar = wgetch(descEntry);
					while (inChar != CONFIRM) {
						if (getcurx(descEntry) == descEntry->_maxx - 2 && getcury(descEntry) < descEntry->_maxy - 1) {
							wmove(descEntry, getcury(descEntry) + 1, 1);
						}

						if (is_allowed_char(inChar) && getcury(descEntry) < descEntry->_maxy - 1) {
							waddch(descEntry, inChar);
							newDesc += inChar;
							wrefresh(descEntry);
						}
						else if (inChar == '\b' && newDesc.size() > 0) {
							if (getcurx(descEntry) == 1 && getcury(descEntry) > 1) {
								wmove(descEntry, getcury(descEntry) - 1, descEntry->_maxx - 2);
							}
							else if (getcurx(descEntry) == 1 && getcury(descEntry) == 1) {
								break;
							}

							mvwaddch(descEntry, getcury(descEntry), getcurx(descEntry) - 1, ' ');
							wmove(descEntry, getcury(descEntry), getcurx(descEntry) - 1);
							newDesc.pop_back();
							wrefresh(descEntry);
						}
						inChar = wgetch(descEntry);
					}

					newQuest.set_desc(newDesc);

					werase(descEntry);
					box(descEntry, NULL, NULL);
					left_align(descEntry, newQuest.get_desc());
					wrefresh(descEntry);
				}
				curs_set(false);
				break;
			case KEY_DOWN:
				if ((unsigned int) dSelection + 1 < wContent.size()) {
					if (dSelection == DESC) {
						mvwaddstr(nqWindow, (dSelection * 3) + 6, 2, wContent[dSelection].c_str());
					}
					else {
						mvwaddstr(nqWindow, (dSelection * 3) + 4, 2, wContent[dSelection].c_str());
					}

					dSelection++;

					wattron(nqWindow, A_STANDOUT);
					if (dSelection == DESC) {
						mvwaddstr(nqWindow, (dSelection * 3) + 6, 2, wContent[dSelection].c_str());
					}
					else {
						mvwaddstr(nqWindow, (dSelection * 3) + 4, 2, wContent[dSelection].c_str());
					}
					wattroff(nqWindow, A_STANDOUT);

					wrefresh(nqWindow);
				}
				break;
			case KEY_UP:
				if (dSelection - 1 >= 0) {
					if (dSelection == DESC) {
						mvwaddstr(nqWindow, (dSelection * 3) + 6, 2, wContent[dSelection].c_str());
					}
					else {
						mvwaddstr(nqWindow, (dSelection * 3) + 4, 2, wContent[dSelection].c_str());
					}
					
					dSelection--;

					wattron(nqWindow, A_STANDOUT);
					if (dSelection == DESC) {
						mvwaddstr(nqWindow, (dSelection * 3) + 6, 2, wContent[dSelection].c_str());
					}
					else {
						mvwaddstr(nqWindow, (dSelection * 3) + 4, 2, wContent[dSelection].c_str());
					}
					wattroff(nqWindow, A_STANDOUT);

					wrefresh(nqWindow);
				}
				break;
			default:
				break;
		}
	}
	return -1;
}

/* Function    : save_quests
 * Description : Saves quests to .sav file
 * Inputs      : quests: quests as Quest vector reference
 * Outputs     : 0 if success, -1 if failure or cancelled
 */
int save_quests(vector<Quest> &quests) {
	std::fstream savefile;
	vector<std::filesystem::path> files;
	int lqSelection = 0;
	int qSelection = 0;
	string line = "";

	// Get .sav files in directory of .exe
	for (const std::filesystem::directory_entry& entry : std::filesystem::directory_iterator("Saves/")) {
		auto path = entry.path();
		if (path.extension().string() == ".sav") {
			files.push_back(path);
		}
	}

	WINDOW* sqWindow = create_window(21, 15, 10, 56);
	mvwaddstr(sqWindow, 1, 2, "SAVE QUESTS");
	mvwhline(sqWindow, 2, 1, ACS_HLINE, 13);

	for (std::size_t i = 0; i < files.size(); i++) {
		if (i == 0) {
			wattron(sqWindow, A_STANDOUT);
			mvwprintw(sqWindow, i + 3, 1, "%s", files[i].filename().string().c_str());
			wattroff(sqWindow, A_STANDOUT);
		}
		else {
			mvwprintw(sqWindow, i + 3, 1, "%s", files[i].filename().string().c_str());
		}
	}

	mvwhline(sqWindow, 17, 1, ACS_HLINE, 13);
	mvwaddstr(sqWindow, 18, 2, "[S]ave File");
	mvwaddstr(sqWindow, 19, 2, "[C]ancel");
	wrefresh(sqWindow);

	while (true) {
		lqSelection = getch();

		if (is_termresized()) {
			resize_term(41, 126);
			curs_set(false);
			refresh_all({ qtWindow, qdWindow, qlWindow, mmWindow, acWindow, sqWindow });
		}

		switch (lqSelection) {
		case CANCEL:
			wclear(sqWindow);
			delwin(sqWindow);
			refresh_all({ qlWindow, qdWindow });
			return -1;
		case SAVE:
			savefile.open(files[qSelection], std::ios::out);

			if (savefile.is_open()) {
				for (std::size_t i = 0; i < quests.size(); i++) {
					savefile << quests[i];
				}
			}

			savefile.close();
			wclear(sqWindow);
			delwin(sqWindow);
			refresh_all({ qlWindow, qdWindow });
			return 0;
		case KEY_DOWN:
			if ((unsigned int) qSelection + 1 < files.size()) {
				mvwaddstr(sqWindow, qSelection + 3, 1, files[qSelection].filename().string().c_str());

				qSelection++;

				wattron(sqWindow, A_STANDOUT);
				mvwaddstr(sqWindow, qSelection + 3, 1, files[qSelection].filename().string().c_str());
				wattroff(sqWindow, A_STANDOUT);

				wrefresh(sqWindow);
			}
			break;
		case KEY_UP:
			if (qSelection - 1 >= 0) {
				mvwaddstr(sqWindow, qSelection + 3, 1, files[qSelection].filename().string().c_str());

				qSelection--;

				wattron(sqWindow, A_STANDOUT);
				mvwaddstr(sqWindow, qSelection + 3, 1, files[qSelection].filename().string().c_str());
				wattroff(sqWindow, A_STANDOUT);

				wrefresh(sqWindow);
			}
			break;
		default:
			break;
		}
	}
	return -1;
}

/* Function    : auto_save
 * Description : Automatically writes to a auto.sav file
 * Inputs      : quests: quests as Quest vector reference
 * Outputs     : 0 if success
 */
int auto_save(vector<Quest> &quests) {
	std::fstream savefile;
	savefile.open("Saves/auto.sav", std::ios::out);

	if (savefile.is_open()) {
		for (std::size_t i = 0; i < quests.size(); i++) {
			savefile << quests[i];
		}
	}

	savefile.close();
	return 0;
}

/* Function    : load_quests
 * Description : Loads quests from .sav file
 * Inputs      : quests: quests as Quest vector pointer
 * Outputs     : 0 if success, -1 if failure or cancelled
 */
int load_quests(vector<Quest> *quests) {
	std::fstream savefile;
	vector<std::filesystem::path> files;
	int lqSelection = 0;
	int qSelection = 0;
	string line = "";

	// Get .sav files in the Saves directory
	for (const std::filesystem::directory_entry& entry : std::filesystem::directory_iterator("Saves/")) {
		std::filesystem::path path = entry.path();
		if (path.extension().string() == ".sav") {
			files.push_back(path);
		}
	}

	WINDOW* lqWindow = create_window(21, 15, 10, 56);
	mvwaddstr(lqWindow, 1, 2, "LOAD QUESTS");
	mvwhline(lqWindow, 2, 1, ACS_HLINE, 13);

	for (std::size_t i = 0; i < files.size(); i++) {
		if (i == 0) {
			wattron(lqWindow, A_STANDOUT);
			mvwprintw(lqWindow, i + 3, 1, "%s", files[i].filename().string().c_str());
			wattroff(lqWindow, A_STANDOUT);
		}
		else {
			mvwprintw(lqWindow, i + 3, 1, "%s", files[i].filename().string().c_str());
		}
	}

	mvwhline(lqWindow, 17, 1, ACS_HLINE, 13);
	mvwaddstr(lqWindow, 18, 2, "[L]oad File");
	mvwaddstr(lqWindow, 19, 2, "[C]ancel");
	wrefresh(lqWindow);
	
	while (true) {
		lqSelection = getch();

		if (is_termresized()) {
			resize_term(41, 126);
			curs_set(false);
			refresh_all({ qtWindow, qdWindow, qlWindow, mmWindow, acWindow, lqWindow });
		}

		switch (lqSelection) {
		case CANCEL:
			wclear(lqWindow);
			delwin(lqWindow);
			refresh_all({ qlWindow, qdWindow });
			return -1;
		case LOAD:
			quests->clear();
			savefile.open(files[qSelection], std::ios::in);

			if (savefile.is_open()) {
				while (getline(savefile, line)) {
					Quest newQuest;
					if (line == "{") {
						savefile >> newQuest;
						quests->push_back(newQuest);
					}
				}
			}

			savefile.close();
			wclear(lqWindow);
			delwin(lqWindow);
			refresh_all({ qlWindow, qdWindow });
			return 0;
		case KEY_DOWN:
			if ((unsigned int) qSelection + 1 < files.size()) {
				mvwaddstr(lqWindow, qSelection + 3, 1, files[qSelection].filename().string().c_str());

				qSelection++;

				wattron(lqWindow, A_STANDOUT);
				mvwaddstr(lqWindow, qSelection + 3, 1, files[qSelection].filename().string().c_str());
				wattroff(lqWindow, A_STANDOUT);

				wrefresh(lqWindow);
			}
			break;
		case KEY_UP:
			if (qSelection - 1 >= 0) {
				mvwaddstr(lqWindow, qSelection + 3, 1, files[qSelection].filename().string().c_str());

				qSelection--;

				wattron(lqWindow, A_STANDOUT);
				mvwaddstr(lqWindow, qSelection + 3, 1, files[qSelection].filename().string().c_str());
				wattroff(lqWindow, A_STANDOUT);

				wrefresh(lqWindow);
			}
			break;
		default:
			break;
		}
	}
	return -1;
}

/* Function    : select_objective
 * Description : Enter objective selection mode for objective and quest completion
 * Inputs      : quest: quest as Quest pointer
 * Outputs     : 0 if success, -1 if failure
 */
int select_objective(Quest *quest) {
	bool allCompleted = true; //all objectives completed?
	bool completed = false;
	int oSelection = 0;
	int soSelection = 0;
	vector<Objective> objectives = quest->get_obj();

	display_details(*quest, oSelection); //update details with current objective selection

	while (true) {
		soSelection = getch();

		if (is_termresized()) {
			resize_term(41, 126);
			curs_set(false);
			refresh_all({ qtWindow, qdWindow, qlWindow, mmWindow, acWindow });
		}

		switch (soSelection) {
		case CONFIRM: //enter key is pressed
			if (objectives.size() == 0) {
				break;
			}

			completed = objectives[oSelection].get_completed();
			objectives[oSelection].set_completed(!completed);
			quest->set_obj(objectives);
			display_details(*quest, oSelection);
			break;
		case KEY_DOWN:
			if ((unsigned int) oSelection + 1 < quest->get_obj().size()) {
				oSelection++;
				display_details(*quest, oSelection);
			}
			break;
		case KEY_UP:
			if (oSelection - 1 >= 0) {
				oSelection--;
				display_details(*quest, oSelection);
			}
			break;
		case KEY_LEFT:
			oSelection = -1;
			display_details(*quest);
			return -1;
		case COMPLETED:
			if (quest->get_obj().size() == 0) { //return if there are no objectives
				return 0;
			}

			allCompleted = true; //reset allCompleted
			for (std::size_t i = 0; i < quest->get_obj().size(); i++) {
				if (quest->get_obj()[i].get_completed() == false) {
					allCompleted = false;
					break; //not all objectives completed
				}
			}
			
			if (allCompleted == false) {
				display_error("A quest can only be completed once all objectives are complete.");
				refresh_all({ qlWindow, qdWindow });
				break;
			}
			else {
				return 0; //exit and complete quest
			}
		default:
			break;
		}
	}
	return -1;
}

/* Function    : show_options
 * Description : Show the options menu
 * Inputs      : void
 * Outputs     : 0 if success, -1 if failure
 */
int show_options() {
	int omSelection = 0;
	int colorSelection = get_color();
	vector<string> textColors = { "BLACK", "BLUE", "GREEN", "CYAN", "RED", "MAGENTA", "YELLOW", "WHITE" };
	
	WINDOW* optionsWindow = create_window(19, 26, (stdscr->_maxy / 2) - (19 / 2), (stdscr->_maxx / 2) - (26 / 2));

	mvwaddstr(optionsWindow, 1, (optionsWindow->_maxx / 2) - (7 / 2), "Options");
	mvwhline(optionsWindow, 2, 1, ACS_HLINE, 24);
	mvwaddstr(optionsWindow, 3, 2, "Text Color:");

	for (std::size_t i = 1; i < textColors.size(); i++) {
		if (i == colorSelection) {
			wattron(optionsWindow, A_STANDOUT);
			mvwaddstr(optionsWindow, i + 2, 14, textColors[i].c_str());
			wattroff(optionsWindow, A_STANDOUT);
		}
		else {
			mvwaddstr(optionsWindow, i + 2, 14, textColors[i].c_str());
		}
	}

	mvwhline(optionsWindow, 10, 1, ACS_HLINE, 24);
	mvwaddstr(optionsWindow, 11, (optionsWindow->_maxx / 2) - (23 / 2), "Designer & Programmer:");
	mvwaddstr(optionsWindow, 12, (optionsWindow->_maxx / 2) - (17 / 2), "Victor Dimitroff");
	mvwaddstr(optionsWindow, 14, (optionsWindow->_maxx / 2) - (20 / 2), "Quest Log (c) v1.01");
	mvwaddstr(optionsWindow, 15, (optionsWindow->_maxx / 2) - (22 / 2), "Victor Dimitroff 2021");
	mvwhline(optionsWindow, 16, 1, ACS_HLINE, 24);
	mvwaddstr(optionsWindow, 17, (optionsWindow->_maxx / 2) - (7 / 2), "[D]one");

	wrefresh(optionsWindow);

	while (true) {
		omSelection = getch();

		if (is_termresized()) {
			resize_term(41, 126);
			curs_set(false);
			refresh_all({ qtWindow, qdWindow, qlWindow, mmWindow, acWindow, optionsWindow });
		}

		switch (omSelection) {
		case DONE:
			set_color(colorSelection);
			wclear(optionsWindow);
			delwin(optionsWindow);
			refresh_all({ qlWindow, qdWindow });
			return 0;
		case KEY_DOWN:
			if ((unsigned int) colorSelection + 1 < textColors.size()) {
				mvwaddstr(optionsWindow, colorSelection + 2, 14, textColors[colorSelection].c_str());

				colorSelection++;

				wattron(optionsWindow, A_STANDOUT);
				mvwaddstr(optionsWindow, colorSelection + 2, 14, textColors[colorSelection].c_str());
				wattroff(optionsWindow, A_STANDOUT);

				wrefresh(optionsWindow);
			}
			break;
		case KEY_UP:
			if ((unsigned int) colorSelection - 1 >= 1) {
				mvwaddstr(optionsWindow, colorSelection + 2, 14, textColors[colorSelection].c_str());
				
				colorSelection--;

				wattron(optionsWindow, A_STANDOUT);
				mvwaddstr(optionsWindow, colorSelection + 2, 14, textColors[colorSelection].c_str());
				wattroff(optionsWindow, A_STANDOUT);

				wrefresh(optionsWindow);
			}
			break;
		default:
			break;
		}
	}
	return -1;
}

/* Function    : main
 * Description : Starts the program
 * Inputs      : void
 * Outputs     : 0 if success
 */
int main() {
	std::fstream configFile;
	vector<Quest> quests;
	
	vector<int> activeQuests = { };
	vector<int> completedQuests = { };
	vector<string> lines = { };
	
	int mmSelection = -1;
	int qSelection = 0;
	int returnCheck = 0;
	char aorcMode = ' ';
	string line = "";
	
	init_curses(); //initialize gui via curses
	get_quests_status(quests, &activeQuests, &completedQuests); //track active and completed indices

	while (true) {
		mmSelection = getch();

		if (is_termresized()) {
			resize_term(41, 126);
			curs_set(false);
			refresh_all({ qtWindow, qdWindow, qlWindow, mmWindow, acWindow });
		}

		switch (mmSelection) {
			case EXIT:
				return 0;
			case ACTIVE:
				g_start = 0;
				qSelection = 0;
				aorcMode = 'a';
				
				clear_all({ qtWindow, qdWindow });
				refresh_all({ qtWindow, qdWindow });
				
				if (view_active_quests(quests, activeQuests, qSelection) == 0) {
					display_details(quests[activeQuests[qSelection]]);
				}
				break;
			case COMPLETED:
				g_start = 0;
				qSelection = 0;
				aorcMode = 'c';

				clear_all({ qtWindow, qdWindow });
				refresh_all({ qtWindow, qdWindow });

				if (view_completed_quests(quests, completedQuests, qSelection) == 0) {
					display_details(quests[completedQuests[qSelection]]);
				}
				break;
			case NEW:
				returnCheck = create_quest(&quests);
				if (returnCheck == 0) {
					activeQuests.push_back(quests.size() - 1);
					auto_save(quests);
					display_confirmation("Quest created successfully.");
				}

				werase(qdWindow);
				box(qdWindow, NULL, NULL);
				refresh_all({ qdWindow });

				g_start = 0;
				qSelection = 0;
				aorcMode = 'a';
				if (view_active_quests(quests, activeQuests, qSelection) == 0) {
					display_details(quests[activeQuests[qSelection]]);
				}
				break;
			case SAVE:
				if (save_quests(quests) == 0) {
					display_confirmation("Quests saved.");
				}
				refresh_all({ qlWindow, qtWindow, qdWindow });
				break;
			case LOAD:
				if (load_quests(&quests) == 0) {
					display_confirmation("Quests loaded successfully.");
				}

				werase(qtWindow);
				werase(qdWindow);
				box(qtWindow, NULL, NULL);
				box(qdWindow, NULL, NULL);
				refresh_all({ qlWindow, qtWindow, qdWindow });

				g_start = 0;
				qSelection = 0;
				aorcMode = 'a';
				get_quests_status(quests, &activeQuests, &completedQuests);
				if (view_active_quests(quests, activeQuests, qSelection) == 0) {
					display_details(quests[activeQuests[qSelection]]);
				}
				break;
			case OPTIONS:
				if (show_options() == 0) {
					display_confirmation("Options applied.");
				}
				refresh_all({ qlWindow, qtWindow, qdWindow });
				break;
			case CONFIRM: //enter key is pressed
				break;
			case KEY_DOWN:
				if (aorcMode == 'a') {
					if ((unsigned int) qSelection + 1 < activeQuests.size()) {
						qSelection++;
						view_active_quests(quests, activeQuests, qSelection);
						display_details(quests[activeQuests[qSelection]]);
					}
				}
				else if (aorcMode == 'c') {
					if ((unsigned int) qSelection + 1 < completedQuests.size()) {
						qSelection++;
						view_completed_quests(quests, completedQuests, qSelection);
						display_details(quests[completedQuests[qSelection]]);
					}
				}
				break;
			case KEY_UP:
				if (aorcMode == 'a') {
					if (qSelection - 1 >= 0) {
						qSelection--;
						view_active_quests(quests, activeQuests, qSelection);
						display_details(quests[activeQuests[qSelection]]);
					}
				}
				else if (aorcMode == 'c') {
					if (qSelection - 1 >= 0) {
						qSelection--;
						view_completed_quests(quests, completedQuests, qSelection);
						display_details(quests[completedQuests[qSelection]]);
					}
				}
				break;
			case KEY_RIGHT:
				if (aorcMode == 'a' && activeQuests.size() > 0 && quests[activeQuests[qSelection]].get_obj().size() >= 0) {
					if (select_objective(&quests[activeQuests[qSelection]]) == 0) {
						quests[activeQuests[qSelection]].set_completed(true);
						auto_save(quests);
						display_confirmation("Quest completed.");
						
						mvwaddstr(acWindow, 1, 5, "[A]CTIVE");
						mvwaddstr(acWindow, 1, (34 / 2) + 3, "[C]OMPLETED");

						werase(qlWindow);
						werase(qtWindow);
						werase(qdWindow);
						box(qlWindow, NULL, NULL);
						box(qtWindow, NULL, NULL);
						box(qdWindow, NULL, NULL);
						refresh_all({ acWindow, qlWindow, qtWindow, qdWindow });

						aorcMode = ' ';
						get_quests_status(quests, &activeQuests, &completedQuests);
					}
				}
				break;
			case KEY_LEFT:
				wclear(qdWindow);
				configFile.open("config.dat", std::ios::in);
				while (getline(configFile, line)) {
					lines.push_back(line);
				}

				for (std::size_t i = 0; i < lines.size(); i++) {
					mvwprintw(qdWindow, (qdWindow->_maxy / 2) + (i - (lines.size() / 2)), (qdWindow->_maxx / 2) - (lines[i].size() / 2), lines[i].c_str());
				}
				wrefresh(qdWindow);
				configFile.close();
				break;
			default:
				break;
		}
	}
	return 0;
}