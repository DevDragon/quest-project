+---------------------------------+
| QUEST LOGGER CODING CONVENTIONS |
|      By: Victor Dimitroff       |
+---------------------------------+

-----------------
TABLE OF CONTENTS
-----------------
+ PURPOSE
+ GENERAL
+ HEADER FILES (.hpp)
+ SOURCE CODE FILES (.cpp)
+ SAVE FILES (.sav)

-------
PURPOSE
-------
+ The purpose of this document is to allow reviewers of my code to understand 
  any significant standards for code structure and conventions that I used in this project.

-------
GENERAL
-------
+ All code files (headers and source code) should be started with a multiline comment detailing:
    - The name of the file
    - Purpose of the file
    - The author of the file
    - Author copyright and disclaimer
+ Multi-line comments should follow this format:
      /* ...
       * ...
       */
+ Single-line comments should not be used for anything longer than a couple of sentences and they
  should follow this format:
      //comment
      // This is a longer comment. It has 2 sentences.
+ #include statements should be sorted by <> vs. "" first, then alphabetically:
      #include <a>
      #include <b>
      #include "a.h"
      #include "b.hpp"
+ Lists of items contained within curly brackets should use the following format:
      { elemA, elemB, elemC }
+ Scope brackets should imploy the following format:
      void foo(void) {
        //content
      }
+ Global variables should be prefixed as follows to better recognize them at a glance:
      g_variable
+ Namespace declarations should be used to the most limited extent. For example,
  "using std::string;" rather than "using namespace std;"
+ References should be formatted as follows:
      &value //when called
      void foo(vector<string>&) //when in a prototype
+ Operators should be surrounded by spaces

-------------------
HEADER FILES (.hpp)
-------------------
+ All header files created for this project should use the .hpp extension
+ Header code should be sorted as follows where applciable:
      #ifndef HEADER_H
      #define HEADER_H

      //includes (sorted <> -> "", then alphabetically)

      //enums (sorted alphabetically)

      //uninitialized variables (sorted extern -> const -> other, then by type size, then alphabetically)

      //initialized variables (sorted extern -> const -> other, then by type size, then alphabetically)

      //structures (sorted alphabetically)

      //function prototypes (sorted by return value size then alphabetically)

      class foo {
      private:
        //variables (headers and source, sorted extern -> const -> other, then by type size, then alphabetically)
      public:
        //constructor prototypes
        //deconstructor prototypes
        //operator overloads
        //other function prototypes sorted by return value size then alphabetically
      };

      #endif

------------------------
SOURCE CODE FILES (.cpp)
------------------------
+ All functions should have a multi-line comment detailing the following:
    - Function name
    - Inputs
    - Outputs
    - Short description of function
+ Header code should be sorted as follows where applciable:
      //includes (sorted <> -> "", then alphabetically)

      //namespace declarations (sorted alphabetically)

      //enums (sorted alphabetically)

      //uninitialized variables (sorted extern -> const -> other, then by type size, then alphabetically)

      //initialized variables (sorted extern -> const -> other, then by type size, then alphabetically)

      //structures (sorted alphabetically)

      //functions (sorted by return value size then alphabetically)

-----------------
SAVE FILES (.sav)
-----------------
+ Save files are structured in the following way:
      {
      q
      (quest id number)
      (completed flag)
      (quest title)
      (quest description)
      [
      o
      (objective id number)
      (completed flag)
      (objective location)
      (objective description)
      ]
      [
      ...
      ]
      }
      {
      ...
      }