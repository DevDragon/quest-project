+--------------------------+
|  QUEST LOGGER -- README  |
|   By: Victor Dimitroff   |
+--------------------------+

-----------------
TABLE OF CONTENTS
-----------------
+ PURPOSE
+ GENERAL
+ VIEWING QUESTS
+ COMPLETING QUESTS AND OBJECTIVES
+ CREATING QUESTS
+ SAVING QUESTS
+ LOADING QUESTS
+ OPTIONS

-------
PURPOSE
-------
+ The purpose of this document is to provide users with a guide to the Quest Logger application.
  This README also provides users with any notes of current limitations in the program.

-------
GENERAL
-------
+ The primary application UI is laid out as follows:
    +-----------------------------------------------------------------------+
    | Active/Completed Selection |            Quest Title Window            |
    +-----------------------------------------------------------------------+
    |                            |                                          |
    |                            |                                          |
    |     Quest List Window      |           Quest Details Window           |
    |                            |                                          |
    |                            |                                          |
    +----------------------------+------------------------------------------+
    |                               Main Menu                               |
    +-----------------------------------------------------------------------+
+ A complete distribution of this application should have the following in the application directory:
    + auto.sav - Autosave file
    + many.sav - Sample save file containg many quests
    + QuestLogger.exe - Application executable
    + README.txt - This file
    + sample.sav - Sample save file
    + save1.sav - Save file 1
    + save2.sav - Save file 2
    + save3.sav - Save file 3
    + save4.sav - Save file 4
    + save5.sav - Save file 5
+ You can add additional save files by creating new blank files with the .sav extension.
+ You can exit the application from the main window by pressing the "x" key.

! Warning: Due to the nature of the GUI library used, resizing the terminal window while the application
  is running may completely distort the UI. If the UI becomes distorted due to this, the application will
  require a restart.

* Note: This application has an autosaving feature. If the program should ever crash or need to be terminated
  due to a problem, you may be able to recover some of your progress with the auto.sav file.

--------------
VIEWING QUESTS
--------------
+ You can view quests by pressing the "a" key to view active quests or "c" to view completed quests. If no
  quests are found, the program will display a message in the quest list window. Otherwise, the application
  will populate the quest list and enter quest viewing mode. Use the "up" and "down" arrow keys to scroll
  through this list.
+ In quest viewing mode, you may use any of the main menu key commands at any time.

--------------------------------
COMPLETING QUESTS AND OBJECTIVES
--------------------------------
+ To complete quests and objectives, first navigate to the active quest of choice. Then, press the "right" 
  arrow key to enter objective selection mode. An option to complete the quest will now appear.
+ If a quest has no objectives, then simply press "c" to complete the quest. A confirmation message will
  appear if the quest has successfully been completed. The quest should now appear under the Completed tab.
+ If a quest has objectives, you may now use the arrow keys to select an objective. Press "enter" to toggle
  an objective as completed, or press "c" to complete a quest.
+ To exit objective selection mode, press the "left" arrow key.

* Note: Quests can only be marked as completed once all of the objectives (if any) are completed.

---------------
CREATING QUESTS
---------------
+ When in quest viewing mode, press "n" to open the quest creation window.
+ In quest creation mode, you must enter an ID number, title, and description for you new quest. Also, the 
  quest creation window will tell you how many objectives are currently associated with your new quest.
+ To enter information into the fields, navigate using the arrow keys to the desired field that you want to
  change then press "enter" to begin data entry. Enter the text or number, then press "enter" again to save 
  the field.

* Note: Entering a field in the quest or objective creation windows will clear any data.

+ Once you have populated all fields and added any objectives to your quest, press "d" to create the new quest.
+ To cancel quest creation, press "c".
+ To add a new objective to the quest (quests may be created without objectives), press "n". This will open the
  objective creation window. Objective IDs are automatically generated, but the user must define a location and
  description for the new objective. Press "c" to cancel and return to quest creation. Press "d" to add your new
  objective to the quest.c

! Warning: Quests and objectives may only be created if all fields are populated. However, quests may be created
  without any quests.
! Warning: Quest ID must be unique. If another quest currently exists in the system with the entered ID, the 
  application will throw an error and clear the field.

-------------
SAVING QUESTS
-------------
+ When in quest viewing mode, press "s" to open the saving window.
+ The quest saving window will list all save files in the same directory as the application executable. Use the arrow
  keys to navigate to the desired file, then press "s" to save over that file.
+ To cancel quest saving, press "c".
+ Upon successful saving, a confirmation message will appear.

! Warning: Only a limited number of save files will fit in the window. If you have added many new save files, they may not
  all appear.

--------------
LOADING QUESTS
--------------
+ When in quest viewing mode, press "l" to open the quest loading window.
+ The quest loading window will list all save files in the same directory as the application executable. Use the arrow
  keys to navigate to the desired file, then press "l" to load that file.
+ To cancel quest loading, press "c".
+ Upon successful loading, a confirmation message will appear.

! Warning: Loading a quest will not save your current progress. If you do not want to lose any data, make sure that you
  save your current quests before loading.

-------
OPTIONS
-------
+ When in quest viewing mode, press "o" to open the options window.
+ Use the arrow keys to select your desired text color, then press 'd' to save your preference and close the window.
+ The options window contains the application credits and copyright information
