# Makefile
CC=g++
CFLAGS=-I.
DEPS = quest.h

%.o: %.cpp $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

make: main.o
	$(CC) -o quest_demo main.o