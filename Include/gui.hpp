/* gui.hpp
 *
 * This is the header declaring the variables and functions associated with the program UI.
 * Also, this file is where the extern variables that refer to the primary windows are located,
 * allowing the quest_project code to directly interact with these windows.
 *
 * Code (c) Victor Dimitroff 2021
 * No part of this code may be copied or altered without explicit permission from the
 * copyright holder.
 */

#ifndef GUI_H
#define GUI_H

#include <string>
#include <vector>
#include "curses.h"
#include "quest.hpp"

enum colors { ERROR = 8, CONFIRMATION };

extern WINDOW* acWindow;
extern WINDOW* mmWindow;
extern WINDOW* qdWindow;
extern WINDOW* qlWindow;
extern WINDOW* qtWindow;

const unsigned short MAX_ALERT_WIDTH = 75;
const unsigned short MAX_LIST_SIZE = 17;

int clear_all(std::vector<WINDOW*>);
int display_confirmation(std::string);
int display_details(Quest&);
int display_details(Quest&, int);
int display_error(std::string);
int get_color(void);
int init_curses(void);
int left_align(WINDOW*, std::string);
int refresh_all(std::vector<WINDOW*>);
int set_color(int);
WINDOW* create_window(int, int, int, int);
std::string format_title_row(Id, std::string);
std::vector<std::string> left_align_objective(std::string, unsigned int, const std::string&);
std::vector<std::string> left_align_str(std::string);

#endif