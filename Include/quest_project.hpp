/* quest_project.hpp
 * 
 * This is the header for the main code file, detailing prototypes for the primary
 * program implementation. Also, this file contains the enums for the menu selections
 * as well as the const for the maximum length of description text.
 * 
 * Code (c) Victor Dimitroff 2021
 * No part of this code may be copied or altered without explicit permission from the
 * copyright holder.
 */

#ifndef QUEST_PROJECT_H
#define QUEST_PROJECT_H

#include <string>
#include <vector>
#include "curses.h"
#include "quest.hpp"

enum dOptions { ID, TITLELOC, DESC };
enum mmOptions { EXIT = 'x', ACTIVE = 'a', COMPLETED = 'c', NEW = 'n', SAVE = 's', LOAD = 'l', OPTIONS = 'o', CONFIRM = 10 };
enum nmOptions { CANCEL = 'c', NEWOB = 'n', DONE = 'd' };

const int MAX_CHARS_LEN = 100;

bool check_unique_qid(std::vector<Quest>*, int);
bool is_allowed_char(int);
int auto_save(std::vector<Quest>&);
int create_objective(std::vector<Objective>*, WINDOW*);
int create_quest(std::vector<Quest>*);
int get_quests_status(std::vector<Quest>&, std::vector<int>*, std::vector<int>*);
int load_quests(std::vector<Quest>*);
int save_quests(std::vector<Quest>&);
int select_objective(Quest*);
int show_options(void);
int view_active_quests(std::vector<Quest>&, std::vector<int>, int);
int view_completed_quests(std::vector<Quest>&, std::vector<int>, int);

#endif