/* quest.hpp
 *
 * This is the header declaring the Quest object.
 *
 * Code (c) Victor Dimitroff 2021
 * No part of this code may be copied or altered without explicit permission from the
 * copyright holder.
 */

#ifndef QUEST_H
#define QUEST_H

#include <vector>
#include "objective.hpp"

class Quest {
	private:
		bool completed;
		Id qid;
		std::string desc;
		std::string title;
		std::vector<Objective> objectives;
		
	public:
		Quest(); //default constructor
		Quest(Id, bool, std::string, std::string, std::vector<Objective>); //complete constructor

		friend std::ostream& operator<<(std::ostream&, const Quest&);
		friend std::istream& operator>>(std::istream&, Quest&);

		void set_completed(bool);
		void set_desc(std::string);
		void set_obj(const std::vector<Objective>&);
		void set_qid(Id);
		void set_title(std::string);

		bool get_completed(void);
		Id get_qid(void);
		std::string get_desc(void);
		std::string get_title(void);
		std::vector<Objective> get_obj(void);
	
};

#endif