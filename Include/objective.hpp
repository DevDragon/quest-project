/* objective.hpp
 *
 * This is the header declaring the objective object. Also, this file contains 
 * the declaration for the ID structure used by Objectives and Quests.
 *
 * Code (c) Victor Dimitroff 2021
 * No part of this code may be copied or altered without explicit permission from the
 * copyright holder.
 */

#ifndef OBJECTIVE_H
#define OBJECTIVE_H

#include <string>

struct Id {
	char type;
	unsigned int num;
};

class Objective {
private:
	bool completed;
	Id oid;
	std::string desc;
	std::string loc;

public:
	Objective(); //default constructor
	Objective(Id, bool, std::string, std::string); //constructor

	friend std::ostream& operator<<(std::ostream&, const Objective&);
	friend std::istream& operator>>(std::istream&, Objective&);

	void set_completed(bool);
	void set_desc(std::string);
	void set_loc(std::string);
	void set_oid(Id);
	
	bool get_completed(void);
	Id get_oid(void);
	std::string get_desc(void);
	std::string get_loc(void);
};

#endif